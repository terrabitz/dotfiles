call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'
Plug 'vim-scripts/indentpython.vim'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'scrooloose/nerdtree'
nnoremap <C-n> :NERDTreeFocus<CR>

Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
Plug 'kana/vim-smartinput'
Plug 'Chiel92/vim-autoformat'
" Call with :AutoFormat or F3

if has('nvim')
  Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/defx.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

" Plug 'scrooloose/nerdcommenter'

"[count]<leader>cc |NERDComComment|
"Comment out the current line or text selected in visual mode.


"[count]<leader>cn |NERDComNestedComment|
"Same as <leader>cc but forces nesting.


"[count]<leader>c<space> |NERDComToggleComment|
"Toggles the comment state of the selected line(s). If the topmost selected
"line is commented, all selected lines are uncommented and vice versa.


"[count]<leader>cm |NERDComMinimalComment|
"Comments the given lines using only one set of multipart delimiters.


"[count]<leader>ci |NERDComInvertComment|
"Toggles the comment state of the selected line(s) individually.


"[count]<leader>cs |NERDComSexyComment|
"Comments out the selected lines ``sexily''


"[count]<leader>cy |NERDComYankComment|
"Same as <leader>cc except that the commented line(s) are yanked first.


"<leader>c$ |NERDComEOLComment|
"Comments the current line from the cursor to the end of line.


"<leader>cA |NERDComAppendComment|
"Adds comment delimiters to the end of line and goes into insert mode between
"them.


"|NERDComInsertComment|
"Adds comment delimiters at the current cursor position and inserts between.
"Disabled by default.


"<leader>ca |NERDComAltDelim|
"Switches to the alternative set of delimiters.


"[count]<leader>cl
"[count]<leader>cb    |NERDComAlignedComment|
"Same as |NERDComComment| except that the delimiters are aligned down the
"left side (<leader>cl) or both sides (<leader>cb).


"[count]<leader>cu |NERDComUncommentLine|
"Uncomments the selected line(s).

Plug 'christoomey/vim-tmux-navigator'
Plug 'andrewstuart/vim-kubernetes'
Plug 'fatih/vim-go', { 'do': ':15verbose GoUpdateBinaries' }

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'majutsushi/tagbar'
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'
" surround.vim
" 
" Surround.vim is all about "surroundings": parentheses, brackets, quotes,
" XML tags, and more.  The plugin provides mappings to easily delete,
" change and add such surroundings in pairs.
" 
" It's easiest to explain with examples.  Press `cs"'` inside
" 
"     "Hello world!"
" 
" to change it to
" 
"     'Hello world!'
" 
" Now press `cs'<q>` to change it to
" 
"     <q>Hello world!</q>
" 
" To go full circle, press `cst"` to get
" 
"     "Hello world!"
" 
" To remove the delimiters entirely, press `ds"`.
" 
"     Hello world!
" 
" Now with the cursor on "Hello", press `ysiw]` (`iw` is a text object).
" 
"     [Hello] world!
" 
" Let's make that braces and add some space (use `}` instead of `{` for no
" space): `cs]{`
" 
"     { Hello } world!
" 
" Now wrap the entire line in parentheses with `yssb` or `yss)`.
" 
"     ({ Hello } world!)
" 
" Revert to the original text: `ds{ds)`
" 
"     Hello world!
" 
" Emphasize hello: `ysiw<em>`
" 
"     <em>Hello</em> world!
" 
" Finally, let's try out visual mode. Press a capital V (for linewise
" visual mode) followed by `S<p class="important">`.
" 
"     <p class="important">
"       <em>Hello</em> world!
"     </p>
" 
" This plugin is very powerful for HTML and XML editing, a niche which
" currently seems underfilled in Vim land.  (As opposed to HTML/XML
" *inserting*, for which many plugins are available).  Adding, changing,
" and removing pairs of tags simultaneously is a breeze.
" 
" The `.` command will work with `ds`, `cs`, and `yss` if you install
" [repeat.vim](https://github.com/tpope/vim-repeat).
"
" * di" - Delete everything inside of quotes
Plug 'tpope/vim-surround'
Plug 'roxma/vim-paste-easy'
Plug 'PProvost/vim-ps1'
Plug 'vim-scripts/vim-vagrant'
" Plug 'tmhedberg/SimpylFold'
" zo - open one fold
" zO - open all folds under cursor recursively
" zc - close one fold
" zC - close all folds under cursor recursively
" zR - open all folds

call plug#end()

set nocompatible              " be iMproved, required
filetype off                  " required

" Set tabbing
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set pastetoggle=<F3>


" Set main configurations
set showmatch
set ignorecase
set incsearch
set nopaste
set number
set undolevels=1000

" Set code folding
" set foldmethod=indent
" set foldlevel=99
" nnoremap <space> za

colo elflord
syntax on

" Specify all key mappings
let mapleader = ","
let maplocalleader = ","
set shell=bash\ --norc
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
noremap <c-d> dd
vnoremap <leader>' <esc>`<i'<esc>`>a'<esc>
vnoremap <leader>" <esc>`<i"<esc>`>a"<esc>
inoremap jk <esc>
nnoremap J <c-f>
nnoremap K <c-b>
nnoremap <C-j>  L
nnoremap <C-k>  H
nnoremap H  0
nnoremap L  $
inoremap <C-o> <esc>o
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
nnoremap <leader>gr :w<cr> :GoRun<cr>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cnoremap w!! w !sudo tee > /dev/null %

" Specify all commands that should not be used
inoremap <esc> <nop>
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>


" Specify all abbreviations
iabbrev adn and
iabbrev waht what
iabbrev tehn then
iabbrev @@ trevor.taubitz@gmail.com

"""""""""" Configure Plugins
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

let python_highlight_all=1
syntax on

let g:arduino_dir = '/opt/arduino-1.8.5'
let g:arduino_auto_baud = 1
let g:arduino_serial_baud = 9600

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" close window if NERDtree is the only one left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeShowHidden = 1

call deoplete#custom#option('omni_patterns', { 'go': '[^. *\t]\.\w*' })
let g:deoplete#enable_at_startup = 1
"""""""""" Specify all autocmds

" Python-specific shortcuts
augroup python
    autocmd FileType python nnoremap <buffer> <localleader>c I#<esc>
    autocmd FileType python iabbrev <buffer> _main if __name__ == "__main__":<cr><tab>
    au BufNewFile,BufRead *.py
                \ set tabstop=4       |
                \ set softtabstop=4   |
                \ set shiftwidth=4    |
                \ set textwidth=79    |
                \ set expandtab       |
                \ set autoindent      |
                \ set fileformat=unix |
    " au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
augroup END

" JS-specific commands
augroup js
    autocmd FileType javascript nnoremap <buffer> <localleader>c I//<esc>
augroup END

" YML-specific command
augroup yml
    " autocmd FileType yml set tabstop=2
    " autocmd FileType yml set shiftwidth=2
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
augroup END

au BufRead,BufNewFile *.pde set filetype=arduino
au BufRead,BufNewFile *.ino set filetype=arduino
au BufRead,BufNewFile *.cpp set filetype=arduino

function! MyStatusLine()
    let port = arduino#GetPort()
    let line = '%f [' . g:arduino_board . '] [' . g:arduino_programmer . ']'
    if !empty(port)
        let line = line . ' (' . port . ':' . g:arduino_serial_baud . ')'
    endif
    return line
endfunction
augroup arduino
    autocmd FileType arduino setl statusline=%!MyStatusLine()
augroup END
