#!/bin/bash

SSH_DIR=~/.ssh
if [ ! -d $SSH_DIR ]; then
    mkdir $SSH_DIR
fi
curl https://gitlab.com/terrabitz.keys -o "$SSH_DIR/authorized_keys"
