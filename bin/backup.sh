#!/bin/bash
# Runs Restic backup on a schedule via cron, emails with status
# 1. add a cred file with your repo logins to /etc/restic/cred

FROM="restic@$(hostname)"
EMAIL="backups@hackandsla.sh"
LOG="/var/log/restic.log"
RDIR="$HOME/.config/restic"
export RESTIC_REPOSITORY="rclone:Wasabi:restic-hackandslash"
export RESTIC_PASSWORD_FILE="${RDIR}/cred"


log() { 
    echo -e "$(date "+%m%d%Y_%H%M%S"): ${1}" | tee -a $LOG
}

notify() {
    echo -e "${1}" | mail -r "${FROM}" -s "Errors running Restic Backup on host: $(hostname)" "${EMAIL}" 
}

cd $RDIR

echo -e "\n" | tee -a $LOG

if [ ! -f cred ]
then
	log "${RDIR}/cred file not present, exiting..\n\n"
	exit 1
fi

log "starting backup.."

msg=$(restic backup --files-from include --exclude-file exclude | tee $LOG)
echo $msg

if [ $? -eq 1 ]
then
    notify "[restic backup]\n${msg}"
    log "${msg}\n-----------------------------------------"
    exit 1
fi

msg=$(restic check | tee $LOG)

if [ $? -eq 1 ]
then
    notify "[restic check]\n${msg}"
    log "${msg}\n--------------------------------------"
    exit 1
fi

log "end of run\n-----------------------------------------\n\n"

# notify OK
echo -e "Snapshot complete" | mail -r "${FROM}" -s "Restic Backup OK on: $(hostname)" ${EMAIL}

