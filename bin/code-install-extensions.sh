#!/usr/bin/env bash

input_file="${HOME}/.config/Code/my-extensions.txt"
args=""
while IFS= read -r line; do
   args+="--install-extension $line "
done < "${input_file}"

code $args
