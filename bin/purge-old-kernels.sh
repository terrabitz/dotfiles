#!/usr/bin/env bash

# From https://askubuntu.com/questions/1207958/error-24-write-error-cannot-write-compressed-block
dpkg -l | egrep "linux-(signed|modules|image|headers)" | grep -v $(uname -r | cut -d - -f 1) | awk {'print $2'} | xargs sudo apt purge -y

