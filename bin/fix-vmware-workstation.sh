#!/usr/bin/env bash

# This script requires that you download the vmware-host-modules project:
# https://github.com/mkubecek/vmware-host-modules
#
# You also need to make sure you checkout the appropriate tag (e.g.
# workstation-15.5.6)

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

main() {
    assert_commands_exists vmware-modconfig make

    cd ~/Software/vmware-host-modules
    make
    sudo make install

    sudo vmware-modconfig --console --install-all

    echo -e "Modules are installed. Reboot system to take effect."
}

assert_commands_exists() {
    local missing_commands=0
    for arg; do
        if [[ ! "$(command -v "${arg}")" ]]; then
            log_error "Command '${arg}' not found"
            missing_commands=1
        fi
    done

    if (( missing_commands == 1 )); then
        echo "Please install all required packages and try again" >&2
        exit 1
    fi
}

log_error() {
    echo -e "[$(red -)] $*" >&2
}

red() {
    echo "$(tput setaf 1)$*$(tput sgr0)"
}

main "$@"