#!/usr/bin/env bash

# Example crontab entry:
# 16 1 * * * PIHOLE_SSH_ADDRESS="username@pi-address" PIHOLE_BACKUP_DIR="/path/to/backups" PIHOLE_SSH_KEY="/path/to/key" /path/to/backup_pi.sh > /tmp/cron_backup_pi.log 2>&1

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

main() {
    assert_commands_exist ssh rsync
    assert_vars_exist PIHOLE_SSH_ADDRESS PIHOLE_SSH_KEY PIHOLE_BACKUP_DIR

    echo "SSH Address: ${PIHOLE_SSH_ADDRESS}"
    echo "SSH Key: ${PIHOLE_SSH_KEY}"
    echo "Backup Directory: ${PIHOLE_BACKUP_DIR}"

    local current_date
    current_date=$(date +"%Y-%m-%d")

    local filename="backup-${current_date}.tar.gz"

    # shellcheck disable=SC2029
    ssh -T "${PIHOLE_SSH_ADDRESS}" -i "${PIHOLE_SSH_KEY}" "pihole -a -t ${filename}"

    scp -i "${PIHOLE_SSH_KEY}" "${PIHOLE_SSH_ADDRESS}:${filename}" "${PIHOLE_BACKUP_DIR}" > /dev/null

    echo "Backup successfully copied to: ${PIHOLE_BACKUP_DIR}/${filename}"
}

show_usage_and_exit() {
    echo "Usage: $0"
    exit 1
}

assert_commands_exist() {
    local missing_commands=0
    for arg; do
        if [[ ! "$(command -v "${arg}")" ]]; then
            log_error "Command '${arg}' not found"
            missing_commands=1
        fi
    done

    if (( missing_commands == 1 )); then
        echo "Please install all required packages and try again" >&2
        exit 1
    fi
}

# Adapted from https://stackoverflow.com/questions/60652528/checking-if-bash-environment-variables-exist-using-a-function/60652618#60652618
assert_vars_exist() {
    local missing_args=0
    for arg; do
      if [[ -z ${!arg+set} ]]; then
        log_error "Missing environment variable: ${arg}"
        missing_args=1
      fi
    done

    if (( missing_args == 1 )); then
        echo "Please specify all required vars" >&2
        exit 1
    fi
}

log_error() {
    echo -e "[$(red -)] $*" >&2
}

red() {
    echo "$(tput setaf 1)$*$(tput sgr0)"
}


main "$@"