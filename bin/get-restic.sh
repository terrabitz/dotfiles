#!/bin/bash

TMP_DIR="/tmp/restic"
DOWNLOAD_URL=$(curl https://api.github.com/repos/restic/restic/releases/latest  | jq -r '.assets[] | select(contains({name:"linux_amd64"})) | .browser_download_url')
mkdir -p $TMP_DIR
wget $DOWNLOAD_URL -P $TMP_DIR
FILE=$(ls $TMP_DIR)
bunzip2 $TMP_DIR/$FILE
BINARY=$(ls $TMP_DIR)
mv $TMP_DIR/$BINARY ~/bin/restic
rm -rf $TMP_DIR
chmod +x ~/bin/restic
