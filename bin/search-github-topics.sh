#!/usr/bin/env bash

function assert_commands_exists() {
    MISSING_COMMANDS=0
    for var in "$@"; do
        if [ ! "$(command -v "$var")" ]; then
            echo "command '$var' not found"
            MISSING_COMMANDS=1
        fi
    done

    if [ $MISSING_COMMANDS == 1 ]; then
        exit 1
    fi
}

function print_help() {
HELP=$(cat << EOF
At least one topic must be provided\n
\n
Example:\n
\n
$0 hacktoberfest go\n
EOF
)

echo -e "$HELP"
}

assert_commands_exists curl jq

if [ "$#" -eq 0 ]; then
    print_help
    exit 1
fi

QUERY=""

for TOPIC in "$@"; do
    if [ "${#QUERY}" -ne 0 ]; then
        QUERY="${QUERY}+"
    fi
    QUERY="${QUERY}topic:${TOPIC}"
done

URL="https://api.github.com/search/repositories?q=$QUERY"

curl -s -H "Accept: application/vnd.github.mercy-preview+json" "$URL" | jq '.items[] | {url:.url, description:.description}'