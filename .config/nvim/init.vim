call plug#begin('~/.vim/plugged')

"Plug 'vim-scripts/indentpython.vim'
"Plug 'kana/vim-smartinput'
"Plug 'tmhedberg/SimpylFold'
" zo - open one fold
" zO - open all folds under cursor recursively
" zc - close one fold
" zC - close all folds under cursor recursively
" zR - open all folds


Plug 'tpope/vim-sensible'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'scrooloose/nerdtree'
nnoremap <C-n> :NERDTreeFocus<CR>

Plug 'ryanoasis/vim-devicons'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
Plug 'Chiel92/vim-autoformat'
" Call with :AutoFormat or F3

if has('nvim')
  Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/defx.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'scrooloose/nerdcommenter'

"[count]<leader>cc |NERDComComment|
"Comment out the current line or text selected in visual mode.


"[count]<leader>cn |NERDComNestedComment|
"Same as <leader>cc but forces nesting.


"[count]<leader>c<space> |NERDComToggleComment|
"Toggles the comment state of the selected line(s). If the topmost selected
"line is commented, all selected lines are uncommented and vice versa.


"[count]<leader>cm |NERDComMinimalComment|
"Comments the given lines using only one set of multipart delimiters.


"[count]<leader>ci |NERDComInvertComment|
"Toggles the comment state of the selected line(s) individually.


"[count]<leader>cs |NERDComSexyComment|
"Comments out the selected lines ``sexily''


"[count]<leader>cy |NERDComYankComment|
"Same as <leader>cc except that the commented line(s) are yanked first.


"<leader>c$ |NERDComEOLComment|
"Comments the current line from the cursor to the end of line.


"<leader>cA |NERDComAppendComment|
"Adds comment delimiters to the end of line and goes into insert mode between
"them.


"|NERDComInsertComment|
"Adds comment delimiters at the current cursor position and inserts between.
"Disabled by default.


"<leader>ca |NERDComAltDelim|
"Switches to the alternative set of delimiters.


"[count]<leader>cl
"[count]<leader>cb    |NERDComAlignedComment|
"Same as |NERDComComment| except that the delimiters are aligned down the
"left side (<leader>cl) or both sides (<leader>cb).


"[count]<leader>cu |NERDComUncommentLine|
"Uncomments the selected line(s).

Plug 'christoomey/vim-tmux-navigator'
Plug 'andrewstuart/vim-kubernetes'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
let g:go_auto_sameids = 1
let g:go_updatetime = 100
let g:go_fmt_command = "goimports"
let g:go_addtags_transform = "snakecase"
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1

Plug 'dense-analysis/ale'
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'

" Enable integration with airline.
let g:airline#extensions#ale#enabled = 1

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

Plug 'majutsushi/tagbar'
Plug 'tpope/vim-surround'
" surround.vim
"
" Surround.vim is all about "surroundings": parentheses, brackets, quotes,
" XML tags, and more.  The plugin provides mappings to easily delete,
" change and add such surroundings in pairs.
"
" It's easiest to explain with examples.  Press `cs"'` inside
"
"     "Hello world!"
"
" to change it to
"
"     'Hello world!'
"
" Now press `cs'<q>` to change it to
"
"     <q>Hello world!</q>
"
" To go full circle, press `cst"` to get
"
"     "Hello world!"
"
" To remove the delimiters entirely, press `ds"`.
"
"     Hello world!
"
" Now with the cursor on "Hello", press `ysiw]` (`iw` is a text object).
"
"     [Hello] world!
"
" Let's make that braces and add some space (use `}` instead of `{` for no
" space): `cs]{`
"
"     { Hello } world!
"
" Now wrap the entire line in parentheses with `yssb` or `yss)`.
"
"     ({ Hello } world!)
"
" Revert to the original text: `ds{ds)`
"
"     Hello world!
"
" Emphasize hello: `ysiw<em>`
"
"     <em>Hello</em> world!
"
" Finally, let's try out visual mode. Press a capital V (for linewise
" visual mode) followed by `S<p class="important">`.
"
"     <p class="important">
"       <em>Hello</em> world!
"     </p>
"
" This plugin is very powerful for HTML and XML editing, a niche which
" currently seems underfilled in Vim land.  (As opposed to HTML/XML
" *inserting*, for which many plugins are available).  Adding, changing,
" and removing pairs of tags simultaneously is a breeze.
"
" The `.` command will work with `ds`, `cs`, and `yss` if you install
" [repeat.vim](https://github.com/tpope/vim-repeat).
Plug 'roxma/vim-paste-easy'
Plug 'PProvost/vim-ps1'
Plug 'vim-scripts/vim-vagrant'
Plug 'luochen1990/rainbow'
let g:rainbow_active = 1
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='violet'
Plug 'airblade/vim-gitgutter'
set updatetime=100
" [c - next hunk
" ]c - previous hunk
" <leader>hp - preview hunk
" <leader>hs - stage hunk
" <leader>hu - undo hunk
Plug 'wincent/terminus'
Plug 'thirtythreeforty/lessspace.vim'
Plug 'TaDaa/vimade'
" Track the engine.
Plug 'SirVer/ultisnips'

" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
Plug 'ervandew/supertab'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
"let g:UltiSnipsExpandTrigger="<CR>"
"let g:UltiSnipsJumpForwardTrigger="<tab>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"let g:ulti_expand_res = 0 "default value, just set once
"function! Ulti_Expand_and_getRes()
    "call UltiSnips#ExpandSnippet()
    "return g:ulti_expand_res
"endfunction


"inoremap <CR> (Ulti_Expand_and_getRes() > 0)?"":"\n"<CR>
let g:ycm_key_list_select_completion=["<tab>"]
let g:ycm_key_list_previous_completion=["<S-tab>"]

let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"
let g:UltiSnipsExpandTrigger="<nop>"
let g:ulti_expand_or_jump_res = 0
function! <SID>ExpandSnippetOrReturn()
  let snippet = UltiSnips#ExpandSnippetOrJump()
  if g:ulti_expand_or_jump_res > 0
    return snippet
  else
    return "\<CR>"
  endif
endfunction
inoremap <expr> <CR> pumvisible() ? "<C-R>=<SID>ExpandSnippetOrReturn()<CR>" : "\<CR>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

Plug 'majutsushi/tagbar'
nnoremap <F8> :TagbarToggle<CR>

Plug 'lambdalisue/suda.vim'

call plug#end()

set nocompatible              " be iMproved, required
filetype off                  " required

" Set tabbing
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set pastetoggle=<F3>


" Set main configurations
set showmatch
set ignorecase
set incsearch
set nopaste
"set number
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
set undolevels=1000
colorscheme murphy

" Set code folding
" set foldmethod=indent
" set foldlevel=99
" nnoremap <space> za

syntax on

" Specify all key mappings
let mapleader = ","
let maplocalleader = ","
set shell=bash
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
noremap <c-d> dd
vnoremap <leader>' <esc>`<i'<esc>`>a'<esc>
vnoremap <leader>" <esc>`<i"<esc>`>a"<esc>
inoremap jk <esc>
vnoremap jk <esc>
nnoremap J <c-f>
nnoremap K <c-b>
nnoremap <C-j>  L
nnoremap <C-k>  H
nnoremap H  0
nnoremap L  $
"inoremap <C-o> <esc>o
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
nnoremap <leader>gr :w<cr> :GoRun<cr>
tnoremap <Esc> <C-\><C-n>
map <C-t><C-k> :tabr<cr>
map <C-t><C-j> :tabl<cr>
map <C-t><C-h> :tabp<cr>
map <C-t><C-l> :tabn<cr>
vnoremap <C-c> :w !xclip -i -sel c<CR><CR>

" Allow saving of files as sudo when I forgot to start vim using sudo.
"cnoremap w!! w !sudo tee > /dev/null %
cnoremap w!! w suda://%

" Some keybindings that are default but hard to remember
" <CTRL-O> - Go back to previous file
" <CTRL-I> - Go forward to file
" <C-r> - Redo a change

" Specify all commands that should not be used
inoremap <esc> <nop>
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>


" Specify all abbreviations
iabbrev adn and
iabbrev waht what
iabbrev tehn then
iabbrev @@ trevor.taubitz@gmail.com

"""""""""" Configure Plugins
let python_highlight_all=1
syntax on

let g:arduino_dir = '/opt/arduino-1.8.5'
let g:arduino_auto_baud = 1
let g:arduino_serial_baud = 9600

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" close window if NERDtree is the only one left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeShowHidden = 1

call deoplete#custom#option('omni_patterns', { 'go': '[^. *\t]\.\w*' })

"""""""""" Specify all autocmds

" Python-specific shortcuts
augroup python
    autocmd FileType python nnoremap <buffer> <localleader>c I#<esc>
    autocmd FileType python iabbrev <buffer> _main if __name__ == "__main__":<cr><tab>
    au BufNewFile,BufRead *.py
                \ set tabstop=4       |
                \ set softtabstop=4   |
                \ set shiftwidth=4    |
                \ set textwidth=79    |
                \ set expandtab       |
                \ set autoindent      |
                \ set fileformat=unix |
    " au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
augroup END

" JS-specific commands
augroup js
    autocmd FileType javascript nnoremap <buffer> <localleader>c I//<esc>
augroup END

" YML-specific command
augroup yml
    " autocmd FileType yml set tabstop=2
    " autocmd FileType yml set shiftwidth=2
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
augroup END

au BufRead,BufNewFile *.pde set filetype=arduino
au BufRead,BufNewFile *.ino set filetype=arduino
au BufRead,BufNewFile *.cpp set filetype=arduino

function! MyStatusLine()
    let port = arduino#GetPort()
    let line = '%f [' . g:arduino_board . '] [' . g:arduino_programmer . ']'
    if !empty(port)
        let line = line . ' (' . port . ':' . g:arduino_serial_baud . ')'
    endif
    return line
endfunction
augroup arduino
    autocmd FileType arduino setl statusline=%!MyStatusLine()
augroup END
